
ABOUT MY RIVER CROSSING CHALLENGE GAME :

1) Playing arena consists of a river with some partitions in it. 

2) There are 2 players in the game, one at Top and another at Bottom (2 sides of the river bank). 

3) A player is safe when it is standing on a partition/slab.

4) There are two kinds of obstacles, moving and fixed. The player starts from the 
‘START’ partition and must reach the ‘END’ position for player 1 and the ‘END’ becomes ‘START’, ‘START’ becomes ‘END’ for Player 2. 

5) The moving obstacles move from left to right or right to left. 

6) The player can move up, down, left and right. 

7) Player dies once he/she touches any obstacle. 

8) As player crosses moving obstacle successfully, accrues 10 points and for 
crossing fixed obstacles 5 points. 

9) Only one player is playing at a given point of time. 

10) Your aim is to make players reach the other end of the river. 

11) The player wins the game based on
	a) Points accrued while crossing obstacles
	b)If points accrued are same then the one who crosses in less time is  
		declared as winner.
	c)If however points accrued and time elapsed for both players are same then
		it's a DRAW  game.

12) A player continues to play till it dies.

13) In every succesive round for each player speed of moving obstacles increases by 
    certain pace.



	I HOPE YOU WILL ENJOY MY GAME AND NEXT TIME I WILL TRY TO MAKE IT BETTER 

ABOUT THE GAME CREATOR:

Name:- ARYAN JAIN
College:- INTERNATIONAL INSTITUTE OF INFORMATION TECHNOLOGY
Branch:- CSE (B.tech,First Year)
Address:-GACHIBOWLI,HYDERABAD (TELENGANA)


